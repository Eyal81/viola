package tipz.viola.blocker

import android.graphics.Bitmap
import android.webkit.JavascriptInterface
import android.webkit.WebView
import tipz.viola.tflite.Classifier
import tipz.viola.utils.ImageUtils


class ImageBlocker(
    private val webView: WebView,
    private val nsfwClassifier: Classifier,
    private val imageHistoryMap: HashMap<String, Boolean>
) {

    companion object {
        val NSFW_TITLES = listOf("porn", "sexy", "hentai")
        const val NSFW_SCORE = 0.072
    }

    @JavascriptInterface
    fun blockImages() {
        webView.loadUrl(
            "javascript:(function() {\n" +
                    "\n" +
                    "    function isNsfwImage(img) {\n" +
                    "        var imgSrc = img.src;\n" +
                    "        if (window.blocker && !window.blocker.isNsfwImage(imgSrc)) {\n" +
                    "          img.style.display = \"\";\n" +
                    "        }\n" +
                    "      }\n" +
                    "\n" +
                    "\n" +
                    "    var imgs = document.getElementsByTagName('img');\n" +
                    "    for (var i = 0; i < imgs.length; i++) {\n" +
                    "        imgs[i].style.display = 'none';\n" +
                    "    }\n" +
                    "    for (var i = 0; i < imgs.length; i++) {\n" +
                    "        isNsfwImage(imgs[i]);\n" +
                    "      }\n" +
                    "})();"
        )
    }


    @JavascriptInterface
    fun isNsfwImage(imgSrc: String): Boolean {
        if (imgSrc != "") {
            try {
                if (imageHistoryMap.contains(imgSrc)) {
                    return imageHistoryMap[imgSrc]!!
                }
                val img: Bitmap? = if (imgSrc.startsWith("data:image/")) {
                    ImageUtils.extractBase64FromDataUrl(imgSrc)
                        ?.let { ImageUtils.getBitmapFromBase64(it) }
                } else {
                    ImageUtils.getBitmapFromUrl(imgSrc)
                }
                if (img != null) {
                    var isNsfw = 0f
                    val output = nsfwClassifier.recognizeImage(img, 0)
                    for (title in output) {
                        if (NSFW_TITLES.contains(title.title)) {
                            isNsfw += title.confidence
                        }
                    }
                    println("$imgSrc $isNsfw")
                    imageHistoryMap[imgSrc] = isNsfw > NSFW_SCORE
                    return isNsfw > NSFW_SCORE
                }
            } catch (e: Exception) {
                System.err.println(e)
                System.err.println("image.src = $imgSrc")
            }
            return true


        }
        return false
    }
}


