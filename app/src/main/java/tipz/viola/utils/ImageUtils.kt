package tipz.viola.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class ImageUtils {
    companion object {
        private const val TAG = "ImageUtils"

        // Function to get a Bitmap from a URL
        fun getBitmapFromUrl(urlString: String): Bitmap? {
            var bitmap: Bitmap? = null
            try {
                val url = URL(urlString)
                val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
                connection.doInput = true
                connection.connect()
                val input: InputStream = connection.inputStream
                bitmap = BitmapFactory.decodeStream(input)
            } catch (e: Exception) {
                Log.e(TAG, "Error getting bitmap from URL: $urlString", e)
            }
            return bitmap
        }

        // Function to get a Bitmap from a Base64-encoded string
        fun getBitmapFromBase64(base64String: String): Bitmap? {
            val decodedBytes: ByteArray = Base64.decode(base64String, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
        }

        fun extractBase64FromDataUrl(dataUrl: String): String? {
            val regex = Regex("^data:image\\/[^;]+;base64,")
            return regex.replace(dataUrl, "")
        }


    }
}
